<img src="Images/Screenshot_20230314_SchoolListView.png" width="250">
<img src="Images/Screenshot_20230314_SchoolListViewSearch.png" width="250">
<img src="Images/Screenshot_20230314_SchoolExtendedInfo.png" width="250">

# NYC Schools
Android application written as a coding challenge for an Android Developer position.

## Architecture Pattern

This project follows the Model-View-ViewModel (MVVM) architecture design  Pattern. This breaks down to:

- Model: This layer is responsible for the abstraction of the data sources. Model and ViewModel work together to get and save the data.

- View: The purpose of this layer is to inform the ViewModel about the user’s action. This layer observes the ViewModel and does not contain any kind of application logic.

- ViewModel: It exposes those data streams which are relevant to the View. Moreover, it serves as a link between the Model and the View.

## Repository Layer(2 Data sources)

- Web Service data using Okhttp
- Sqlite data using Room

## Package Structure
- database : Which is used to write queries to get data from sqlite db and for sqlite connection using Room.
- model : Which acts as the viewModel as part of MVVM pattern.
- pojo :  Table representation for the sqlite db.
- repository : Get the data from webservice and load it into db for future use.
- userinterface : Which is used for the screen view related activites, view holders and adapters. 

## Libraries
- AppCompat
- ConstraintLayout
- CardView
- CoreTesting
- Lifecycle
- Material
- Room
- Junit
- Espresso
- AndroidxJunit
- Okhttp
- Gson
