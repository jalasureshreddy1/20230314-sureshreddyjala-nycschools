package com.sureshreddyjalanycschool.userinterface;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sureshreddyjalanycschool.R;
import com.sureshreddyjalanycschool.model.SATScoresViewModel;
import com.sureshreddyjalanycschool.pojo.SATScores;
import com.sureshreddyjalanycschool.pojo.School;

import java.util.Objects;

/**
 * It is used to render the list of NYC schools
 * detailed information for specific school.
 * @author Suresh Jala
 */
public class SchoolActivity extends AppCompatActivity {

    private School school;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);
        Toolbar toolbar = findViewById(R.id.school_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //School object to setup the page
        school = getIntent().getParcelableExtra("School");

        ImageButton directionsButton = findViewById(R.id.directionsButton);
        directionsButton.setOnClickListener(v -> {
            Uri gmmIntentUri = Uri.parse("geo:" + school.getLatitude() + "," + school.getLongitude() + "?q=" + school.getSchool_name());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        });

        ImageButton websiteBUtton = findViewById(R.id.websiteButton);
        websiteBUtton.setOnClickListener(v -> {
            Uri websiteIntentUri = Uri.parse(school.getWebsite());
            Intent websiteIntent = new Intent(Intent.ACTION_VIEW, websiteIntentUri);
            v.getContext().startActivity(Intent.createChooser(websiteIntent, "Browse with"));
        });
        ImageButton callButton = findViewById(R.id.callButton);
        callButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + school.getPhone_number()));
            startActivity(intent);
        });

        ImageButton emailButton = findViewById(R.id.emailButton);
        emailButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setData(Uri.parse("mailto:" + school.getSchool_email()));
            v.getContext().startActivity(Intent.createChooser(intent, "Email School "));
        });

        // Setup UI with School object from intent
        TextView schoolNameTextView = findViewById(R.id.schoolNameText);
        TextView descTextView = findViewById(R.id.descriptionText);
        schoolNameTextView.setText(school.getSchool_name());
        schoolNameTextView.setSingleLine(false);
        schoolNameTextView.setHorizontallyScrolling(false);
        descTextView.setText(school.getOverview_paragraph());
        descTextView.setSingleLine(false);
        descTextView.setHorizontallyScrolling(false);

        // Loads SAT Scores for the School
        SATScoresViewModel satScoresViewModel = new ViewModelProvider(this).get(SATScoresViewModel.class);
        LiveData<SATScores> score = satScoresViewModel.getScoresForSchool(school.getDbn());
        score.observe(this, this::satScoresUpdated);
    }

    /**
     * Updates the SAT Scores once available from DB
     *
     * @param score
     */
    private void satScoresUpdated(SATScores score) {
        if (score != null) {
            TextView satScoresTextView = findViewById(R.id.satScoresTextView);
            satScoresTextView.setText(score.toString());
        }
    }
}