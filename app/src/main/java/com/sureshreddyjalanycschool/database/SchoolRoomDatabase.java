package com.sureshreddyjalanycschool.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.sureshreddyjalanycschool.pojo.SATScores;
import com.sureshreddyjalanycschool.pojo.School;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Used to connect to db and create the repo
 * @author Suresh Jala
 */
@Database(entities = {School.class, SATScores.class}, version = 2, exportSchema = false)
public abstract class SchoolRoomDatabase extends RoomDatabase {

    public abstract SchoolDao schoolDao();
    private static volatile SchoolRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 10;
    // Uses the Executer Service to run DB operations in background and concurrently
    public static final ExecutorService databaseWriteExecutor =  Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    public abstract SATScoresDao satScoresDao();
    /**
     * DB Singleton
     * @param context
     * @return
     */
    public static SchoolRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (SchoolRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                    SchoolRoomDatabase.class, "school_database")
                            .addCallback(sRoomDatabaseCallback)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Handle any setup after DB is created for the first time
     */
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            databaseWriteExecutor.execute(() -> {
                SchoolDao schoolDao = INSTANCE.schoolDao();
                schoolDao.deleteAll();
                SATScoresDao scoresDao = INSTANCE.satScoresDao();
                scoresDao.deleteAll();
            });
        }
    };
}
