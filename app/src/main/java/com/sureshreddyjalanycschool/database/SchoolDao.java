package com.sureshreddyjalanycschool.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.sureshreddyjalanycschool.pojo.School;

import java.util.List;

/**
 * SchoolDao data access object to get data from db
 * @author Suresh Jala
 */
@Dao
public interface SchoolDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<School> school);

    @Query("DELETE FROM school_table")
    void deleteAll();

    @Transaction
    @Query("SELECT * FROM school_table ORDER BY school_name ASC")
    LiveData<List<School>> getSchools();

    @Transaction
    @Query("SELECT * FROM school_table where school_name like :searchString ORDER BY school_name ASC")
    LiveData<List<School>> getSchoolsFiltered(String searchString);
}
