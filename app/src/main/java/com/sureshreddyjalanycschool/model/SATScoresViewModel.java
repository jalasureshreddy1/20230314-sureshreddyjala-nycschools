package com.sureshreddyjalanycschool.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.sureshreddyjalanycschool.pojo.SATScores;
import com.sureshreddyjalanycschool.repository.SchoolRepository;

/**
 * View model for SchoolActivity
 * @author Suresh Jala
 */
public class SATScoresViewModel extends AndroidViewModel {

    private SchoolRepository mRepository;

    public SATScoresViewModel(@NonNull Application application) {
        super(application);
        mRepository = SchoolRepository.getRepository(application.getApplicationContext());
    }

    public LiveData<SATScores> getScoresForSchool(String schoolDBN) { return mRepository.getSATScoresForSchool(schoolDBN);}

}
